{
  inputs = {
    chronch.url = "gitlab:eyesheet/chronch";
    user-service.url = "gitlab:eyesheet/user-service";
    banjomus.url = "gitlab:eyesheet/banjomus";
    web-frontend.url = "gitlab:eyesheet/web-frontend";
    utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
  };
  outputs = { self, chronch, user-service, banjomus, web-frontend, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        pkgsOf = input: input.packages.${system};
        dockerImageOf = input: input.packages.${system}.dockerImage;
      in {
        devShell = pkgs.mkShell {
          CHRONCH_IMAGE_TAG = (dockerImageOf chronch).imageTag;
          USER_SERVICE_IMAGE_TAG = (dockerImageOf user-service).imageTag;
          BANJOMUS_IMAGE_TAG = (dockerImageOf banjomus).imageTag;
          WEB_FRONTEND_IMAGE_TAG = (dockerImageOf web-frontend).imageTag;

          packages = with pkgs; [ openssl ];

          shellHook = ''
            docker load -i ${dockerImageOf chronch}
            docker load -i ${dockerImageOf user-service}
            docker load -i ${dockerImageOf banjomus}
            docker load -i ${dockerImageOf web-frontend}
            [[ -f ./user-prikey ]] || {
              [[ -f ./user-pubkey ]] && rm user-pubkey && echo "Existing user-pubkey exists without private key. Removing it..." 1>&2
              echo "Generating user-prikey..." 1>&2
              openssl genpkey -quiet -out user-prikey -algorithm RSA
            }
            [[ -f ./user-pubkey ]] || {
              echo "Generating user-pubkey..." 1>&2
              openssl pkey -in user-prikey -pubout -out user-pubkey
            }
          '';
        };
      }
    );
}
