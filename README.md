# Example Infrastructure for Eyesheet

This is repository is meant as a working example of how to setup Eyesheet
and its services using Docker Compose for orchestration and Envoy for reverse
proxying. It is currently only being tested for running on single machine,
and it has not tested in production.

## Required setup

The Nix flake devShell will automatically setup cryptographic keys for user
auth, however setup is needed for configuring database passwords. This is
done through environment variables (for example using a .env file). The env
variables are

* CHRONCH_DB_PASSWD
* USER_DB_PASSWD
